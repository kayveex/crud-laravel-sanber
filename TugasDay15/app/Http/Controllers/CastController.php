<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Menjalankan Fungsi Database - Harus Ada Ini !
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create() {
        return view('CRUD.Cast.add');
    }

    public function store(Request $request) {
        // Validasi dari Form, udah bener belum -> Termasuk nama, umur, bio -> dari name = " "
        $request->validate([
            'nama' => 'required|max:45| min:2',
            'umur' => 'required',
            'bio'  => 'required'
        ]);
        // Input Ke DB kita
        $query = DB::table('cast')->insert([
            "nama" => $request->input('nama'),
            "umur" => $request->input('umur'),
            "bio" => $request->input('bio')
        ]);

        // Lempar ke halaman view Tabel nya
        return redirect('/cast');
    }

    public function index () {
        // Mengambil Data dari Tabel
        $cast = DB::table('cast')->get();
        // Lempar ke halaman view Tabel nya
        return view('CRUD.Cast.index', ['cast' => $cast] );
    }

    
    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        // Cara Pintas - Shortcut bikin array asso dri Laravel
        return view('CRUD.Cast.detail', compact('cast'));
        // Cara Manual - Bikin Array Asso
        // return view('CRUD.Cast.detail', ['cast' => $cast] );
    }

    
    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('CRUD.Cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|max:45| min:2',
            'umur' => 'required',
            'bio'  => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request->input('nama'),
                "umur" => $request->input('umur'),
                "bio" => $request->input('bio')
            ]);
        return redirect('/cast');
    }

    public function drop($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }


    // Yang bawah Ini Error?!
    // public function show($id)
    // {
    //     $cast = DB::table('cast') -> find('$id');
    //     return view('CRUD.Cast.detail', ['cast' => $cast] );
    // }

}
