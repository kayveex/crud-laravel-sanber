@extends('layouts.master')

{{-- Bagian Nambahin Judul di HTML --}}
@section('judul')
    Tambah Data - Cast 🤺
@endsection

@section('subjudul')
    🌐 New Tab
@endsection

{{-- Bagian Nambahin Konten --}}
@section('content')
    <div>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Tulis nama lengkap!">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" id="umur" placeholder="Masukkan angka saja!">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" id="bio" cols="100" rows="3"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambahkan</button>
        </form>
    </div>
@endsection

