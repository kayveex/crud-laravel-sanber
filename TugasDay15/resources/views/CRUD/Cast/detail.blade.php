@extends('layouts.master')

{{-- Bagian Nambahin Judul di HTML --}}
@section('judul')
    Detail Data {{$cast->nama}} - Cast 🤺
@endsection

@section('subjudul')
    <strong>🌐 Cast ID :</strong> {{ $cast->id }}
@endsection

@section('content')
    <p class="detail-info"><strong>Nama: </strong>{{$cast->nama}}</p>
    <p class="detail-info"><strong>Umur: </strong>{{$cast->umur}} Tahun</p>
    <p class="detail-info"><strong>Bio: </strong>{{$cast->bio}}</p>
@endsection