@extends('layouts.master')

{{-- Bagian Nambahin Judul di HTML --}}
@section('judul')
    Edit Data {{ $cast->nama }} - Cast 🤺
@endsection

@section('subjudul')
    <strong>🌐 Cast ID :</strong> {{ $cast->id }}
@endsection

@section('content')
<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Tulis nama lengkap!" value="{{$cast->nama}}">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur" id="umur" placeholder="Masukkan angka saja!" value="{{$cast->umur}}">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" id="bio" cols="100" rows="3">{{$cast->bio}}</textarea >
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-outline-primary">Update</button>
    </form>
</div>
@endsection