@extends('layouts.master')

{{-- Bagian Nambahin Judul di HTML --}}
@section('judul')
    Tampil Data - Cast 🤺
@endsection

@section('subjudul')
    🌐 New Tab
@endsection

@section('content')
<table  id="castTable" class="table">
    <thead>
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$item->id}}" class="btn btn-info">Biodata</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-secondary">Edit</a>
                    <input type="submit" class="btn btn-danger my-1" value="Hapus">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="3">
            <td>Data Tidak Tersedia 😢</td>
        </tr>  
        @endforelse              
    </tbody>
  </table>
  <hr>
  <div class="col-md-12 text-center">
    <a href="/cast/create" class="btn btn-lg btn-primary text-center">Tambah</a>
  </div>
@endsection

{{-- Addon For DataTable  --}}
@push('cssAddon')
    <link href="https://cdn.datatables.net/v/bs4/dt-1.13.5/datatables.min.css" rel="stylesheet"/>
@endpush

@push('scripts')
    <script>
    $(function () {
        $("#castTable").DataTable();
    });
    </script>
    <script src="https://cdn.datatables.net/v/bs4/dt-1.13.5/datatables.min.js"></script>
@endpush