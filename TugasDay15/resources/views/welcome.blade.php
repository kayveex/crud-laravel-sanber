@extends('layouts.master')

{{-- Bagian Nambahin Judul di HTML --}}
@section('judul')
    Halaman Utama 🏠
@endsection

@section('subjudul')
    Selamat Datang Di Dashboard 🎉
@endsection

{{-- Bagian Nambahin Konten --}}
@section('content')
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse recusandae reprehenderit, dignissimos excepturi inventore fugit exercitationem distinctio facilis eaque omnis labore, illum sunt repellendus. Enim assumenda vel ut earum nesciunt!
@endsection

