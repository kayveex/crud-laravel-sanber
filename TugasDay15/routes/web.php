<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WebController::class,'getWelcome']);
Route::get('/table', [WebController::class,'getTable']);
Route::get('/data-table', [WebController::class,'getdataTable']);
Route::get('/contact', [WebController::class,'getContact']);

// CRUD SYSTEM - CAST

// CREATE
Route::get('/cast/create', [CastController::class,'create']);
// Buat kirim data ke table cast
Route::post('/cast', [CastController::class,'store']);

// READ
Route::get('/cast', [CastController::class,'index']);
Route::get('/cast/{id}', [CastController::class,'show']);


// UPDATE
Route::get('/cast/{id}/edit', [CastController::class,'edit']);
Route::put('/cast/{id}', [CastController::class,'update']);

// DELETE
Route::delete('/cast/{id}', [CastController::class,'drop']);